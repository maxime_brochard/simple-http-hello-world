# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 
## Conteneurisation via Docker

* créer une image Docker du projet 
``docker build -t eval .``

* lancer le conteneur ``docker run --name eval -it -d -p 8181:8181 eval`` 

* vérifier que le conteneur est lancé ``docker container ls -l`` (status = up X.time)