#
# Build stage
#
FROM maven:3.6.0-jdk-13 AS build
WORKDIR /src
COPY pom.xml pom.xml
COPY . .
RUN mvn clean package
EXPOSE 8181
CMD java -jar target/*.jar
