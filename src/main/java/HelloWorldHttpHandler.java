import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

import java.time.Clock;

public class HelloWorldHttpHandler extends HttpHandler {

    final private Clock clock;

    public HelloWorldHttpHandler(Clock clock) {
        assert clock != null;

        this.clock = clock;
    }

    public void service(Request request, Response response) throws Exception {
        response.setContentType("text/plain");
        String responseBody = getResponseBody();

        response.setContentLength(responseBody.length());
        response.getWriter().write(responseBody);
    }

    public String getResponseBody() {
        return "Hello, world! It is currently " + java.time.LocalTime.now(clock).toString();
    }
}
