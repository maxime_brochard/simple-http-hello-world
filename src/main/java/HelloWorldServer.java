import org.glassfish.grizzly.http.server.HttpServer;

import java.io.IOException;
import java.time.Clock;

public class HelloWorldServer {
    public static void main(String[] args) {
        HttpServer server = HttpServer.createSimpleServer(null, 8181);
        try {
            HelloWorldHttpHandler handler = new HelloWorldHttpHandler(Clock.systemDefaultZone());
            server.getServerConfiguration().addHttpHandler(handler, "/hello");

            server.start();
            System.out.println("Press any key to stop the server...");
            System.in.read();
        } catch (IOException e) {
            System.err.println("Error during server execution");
            e.printStackTrace();
        }
    }
}
